from flask import Flask,request,render_template,url_for
from random import randint

app = Flask(__name__)

@app.route("/machine1")
def machine1():
	'''
	As of now just return any random number from 0 to 100
	This function will be polled by the web to display the output in digital meter
	This could be changed to read any output from a source
	'''
	rint = randint(0,100)
	return str(rint)

@app.route("/machine2")
def machine2():
	return str(randint(0,100))

@app.route("/machine3")
def machine3():
	return str(randint(0,100))

@app.route("/machine4")
def machine4():
	return str(randint(0,100))

@app.route("/machine5")
def machine5():
	return str(randint(0,100))

@app.route("/machine6")
def machine6():
	return str(randint(0,100))

@app.route("/machine7")
def machine7():
	return str(randint(0,100))

@app.route("/machine8")
def machine8():
	return str(randint(0,100))

@app.route("/machine9")
def machine9():
	return str(randint(0,100))

@app.route("/machine10")
def machine10():
	return str(randint(0,100))

@app.route("/")
def main():
	return render_template('googlegauge.html')

if __name__ == "__main__":
	app.run(debug=True)